<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class TeamController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        return $this->sendResponse($teams->toArray(), 'Teams retrieved successfully.');
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $orderBy = (isset($input['orderBy']))?$input['orderBy']:'name_en';
        $orderByAsc = (isset($input['orderByAsc']))?$input['orderByAsc']:'asc';
        $page = (isset($input['page']))?$input['page']-1:0;
        $limit = (isset($input['limit']))?$input['limit']:10;
        $searchBy = (isset($input['searchBy']))?$input['searchBy']:false;
        $searchValue = (isset($input['searchValue']))?$input['searchValue']:false;
        $repTeam = array();
        $Teams = Team::orderBy($orderBy, $orderByAsc)
                        ->where(function ($query) use($searchBy, $searchValue){
                            if($searchBy and $searchValue)
                            $query->where($searchBy, 'like', "%".$searchValue."%");
                        })
                        ->offset($page * $limit)
                        ->limit($limit)
                        ->get();
        $total = Team::orderBy($orderBy, $orderByAsc)
                            ->where(function ($query) use($searchBy, $searchValue){
                                if($searchBy and $searchValue)
                                    $query->where($searchBy, 'like', "%".$searchValue."%");
                            })
                            ->count();
        return $this->sendResponse($Teams->toArray(), $total);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name_ar' => 'required|string|unique:teams|max:120',
            'name_en' => 'required|string|unique:teams|max:120',
            'short_name' => 'required|string|max:20',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }

        $filename = $_FILES['logo']['name'];        
        $path = $request->file('logo')->storeAs(
            'public/teams', $request->name_en."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
        );

        $team = new Team;
        $team->name_ar = $input['name_ar'];
        $team->name_en = $input['name_en'];
        $team->short_name = $input['short_name'];
        $team->logo = str_replace("public/teams","/storage/teams",$path);
        $team->description = $input['description'];
        $team->save();

        return $this->sendResponse($team, 'L\'équipe '.$team->name.'a été bien ajouter.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = Team::find($id);
        if (is_null($team))
            return $this->sendError('Team not found.');
        return $this->sendResponse($team->toArray(), 'L\'équipe '.$team->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::find($id);
        if (is_null($team))
            return $this->sendError('Team not found.');
        $input = $request->all();
        $validator = Validator::make($input, [
            'name_ar' => 'required|string|unique:teams,id,'.$id.'|max:120',
            'name_en' => 'required|string|unique:teams,id,'.$id.'|max:120',
            'short_name' => 'required|string|max:20',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }

        if(isset($_FILES['logo'])){
            $filename = $_FILES['logo']['name'];
            $path = $request->file('logo')->storeAs(
                'public/teams', $request->name_en."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
            );
            $team->logo = str_replace("public/teams","/storage/teams",$path);
        }

        $team->name_ar = $input['name_ar'];
        $team->name_en = $input['name_en'];
        $team->short_name = $input['short_name'];
        $team->description = $input['description'];
        $team->save();

        return $this->sendResponse($team, 'L\'équipe '.$team->name.'a été bien ajouter.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::find($id);
        if (is_null($team))
            return $this->sendError('Team not found.');
        $objet = $team->name_en;
        Team::destroy($id);
        return $this->sendResponse($objet, 'L\'équipe '.$objet.' a été supprimer.');
    }
}
