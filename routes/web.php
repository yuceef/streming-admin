<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Back end
Route::resource('/apiWeb/teams', 'TeamController');
Route::post('/apiWeb/teams/search', 'TeamController@search');

Route::resource('/apiWeb/leagues', 'LeagueController');
Route::post('/apiWeb/leagues/search', 'LeagueController@search');

Route::resource('/apiWeb/channels', 'ChannelController');
Route::post('/apiWeb/channels/search', 'ChannelController@search');

Route::resource('/apiWeb/matchs', 'MatchController');
Route::post('/apiWeb/matchs/search', 'MatchController@search');

Route::resource('/apiWeb/videos', 'VideoController');
Route::post('/apiWeb/videos/search', 'VideoController@search');
Route::post('/apiWeb/videos/generer', 'VideoController@generer');

Route::get('/{var}', 'HomeController@index')->where('var','[^*]*');
//Route::get('/{var}', 'HomeController@index')->where('var','[^*]*');