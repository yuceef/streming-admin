<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class ChannelController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channels = Channel::all();
        return $this->sendResponse($channels->toArray(), 'Channels retrieved successfully.');
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $orderBy = (isset($input['orderBy']))?$input['orderBy']:'name_en';
        $orderByAsc = (isset($input['orderByAsc']))?$input['orderByAsc']:'asc';
        $page = (isset($input['page']))?$input['page']-1:0;
        $limit = (isset($input['limit']))?$input['limit']:10;
        $searchBy = (isset($input['searchBy']))?$input['searchBy']:false;
        $searchValue = (isset($input['searchValue']))?$input['searchValue']:false;
        $repChannel = array();
        $Channels = Channel::orderBy($orderBy, $orderByAsc)
                        ->where(function ($query) use($searchBy, $searchValue){
                            if($searchBy and $searchValue)
                            $query->where($searchBy, 'like', "%".$searchValue."%");
                        })
                        ->offset($page * $limit)
                        ->limit($limit)
                        ->get();
        $total = Channel::orderBy($orderBy, $orderByAsc)
                            ->where(function ($query) use($searchBy, $searchValue){
                                if($searchBy and $searchValue)
                                    $query->where($searchBy, 'like', "%".$searchValue."%");
                            })
                            ->count();
        return $this->sendResponse($Channels->toArray(), $total);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name_ar' => 'required|string|unique:channels|max:120',
            'name_en' => 'required|string|unique:channels|max:120',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'stream' => 'required|string|min:5',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }

        $filename = $_FILES['logo']['name'];        
        $path = $request->file('logo')->storeAs(
            'public/channels', $request->name_en."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
        );

        $channel = new Channel;
        $channel->name_ar = $input['name_ar'];
        $channel->name_en = $input['name_en'];
        $channel->logo = str_replace("public/channels","/storage/channels",$path);
        $channel->stream = $input['stream'];
        $channel->description = $input['description'];
        $channel->ref = $this->genererRef(10);
        $this->writePageHtml($channel);
        $channel->save();

        return $this->sendResponse($channel, 'la chaîne '.$channel->name.'a été bien ajouter.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $channel = Channel::find($id);
        if (is_null($channel))
            return $this->sendError('Channel not found.');
        return $this->sendResponse($channel->toArray(), 'la chaîne '.$channel->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $channel = Channel::find($id);
        if (is_null($channel))
            return $this->sendError('Channel not found.');
        $input = $request->all();
        $validator = Validator::make($input, [
            'name_ar' => 'required|string|unique:channels,id,'.$id.'|max:120',
            'name_en' => 'required|string|unique:channels,id,'.$id.'|max:120',
            'stream' => 'required|string|min:5',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }

        if(isset($_FILES['logo'])){
            $filename = $_FILES['logo']['name'];
            $path = $request->file('logo')->storeAs(
                'public/channels', $request->name_en."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
            );
            $channel->logo = str_replace("public/channels","/storage/channels",$path);
        }

        $channel->name_ar = $input['name_ar'];
        $channel->name_en = $input['name_en'];
        $channel->stream = $input['stream'];
        $channel->description = $input['description'];
        $channel->ref = $this->genererRef(10);
        $this->writePageHtml($channel);
        $channel->save();

        return $this->sendResponse($channel, 'la chaîne '.$channel->name.'a été bien ajouter.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $channel = Channel::find($id);
        if (is_null($channel))
            return $this->sendError('Channel not found.');
        $objet = $channel->name_en;
        Channel::destroy($id);
        return $this->sendResponse($objet, 'la chaîne '.$objet.' a été supprimer.');
    }

    private function genererRef($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return md5($randomString);
    }

    private function writePageHtml($channel){
        $fp = "./channels/".$channel->ref.".html";
        $handle = fopen($fp, "w+");
        $content = $this->getPageHtml($channel);
        fwrite($handle, $content);
        fclose($handle);
    }
    
    private function getPageHtml($channel){
        return '
        <html>
            <head>
                <title>'.$channel->name.'</title>
            </head>
            <body>
                <script src="https://cdn.jsdelivr.net/clappr/latest/clappr.min.js"></script>
                <div id="player"></div>
                <script>
                    window.onload = function() {
                        var player = new Clappr.Player({
                            poster: "https://image.ibb.co/mtaeMc/23517427_1987005781543348_2227911412155866414_n.jpg",

                            source: "'.$channel->stream.'", 
                            parentId: "#player",
                            loop: true,
                            allowUserInteraction: true,
                            watermark: "/images/logo.png", 
                            position: "top-right",
                            watermarkLink: "/",
                            playbackNotSupportedMessage: "Please try on a different browser",
                            useHardwareVideoDecoder: false,
                            crossorigin: "use-credentials",
                            autoPlay: true,
                            playInline: true, // allows inline playback when running on iOS UIWebview
                            recycleVideo: Clappr.Browser.isMobile, // Recycle <video> element only for mobile. (default is false)
                            width: "100%",
                            height: "100%",
                        });
                    }
                </script>
            </body>
        </html>
        ';
    }
}
