<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class VideoController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('store','update','destroy','generer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::all();
        return $this->sendResponse($videos->toArray(), 'Videos retrieved successfully.');
    }

    public function generer(Request $request){
        $input = $request->all();
        foreach($input as $video){
            if(Video::where('url',$video['id']['videoId'])->count()==0){
                $myvideo = new Video;
                $myvideo->url = $video['id']['videoId'];
                $myvideo->title = $video['snippet']['title'];
                $myvideo->description = $video['snippet']['title']." : ".$video['snippet']['description'];
                $myvideo->save();
            }
        }
        return $this->sendResponse([], 'Les videos ont été bien ajouter.');
    }
    public function search(Request $request)
    {
        $input = $request->all();
        $orderBy = (isset($input['orderBy']))?$input['orderBy']:'created_at';
        $orderByAsc = (isset($input['orderByAsc']))?$input['orderByAsc']:'desc';
        $page = (isset($input['page']))?$input['page']-1:0;
        $limit = (isset($input['limit']))?$input['limit']:10;
        $searchBy = (isset($input['searchBy']))?$input['searchBy']:false;
        $searchValue = (isset($input['searchValue']))?$input['searchValue']:false;
        $repVideo = array();
        $Videos = Video::orderBy($orderBy, $orderByAsc)
                        ->where(function ($query) use($searchBy, $searchValue){
                            if($searchBy and $searchValue)
                            $query->where($searchBy, 'like', "%".$searchValue."%");
                        })
                        ->offset($page * $limit)
                        ->limit($limit)
                        ->get();
        $total = Video::orderBy($orderBy, $orderByAsc)
                            ->where(function ($query) use($searchBy, $searchValue){
                                if($searchBy and $searchValue)
                                    $query->where($searchBy, 'like', "%".$searchValue."%");
                            })
                            ->count();
        return $this->sendResponse($Videos->toArray(), $total);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'url' => 'required|string|unique:videos|min:10|max:120',
            'title' => 'required|string|max:120',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }
        $video = new Video;
        $video->url = $input['url'];
        $video->title = $input['title'];
        $video->description = $input['description'];
        $video->save();

        return $this->sendResponse($video, 'La video '.$video->name.'a été bien ajouter.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::find($id);
        if (is_null($video))
            return $this->sendError('Video not found.');
        return $this->sendResponse($video->toArray(), 'La video '.$video->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::find($id);
        if (is_null($video))
            return $this->sendError('Video not found.');
        $input = $request->all();
        $validator = Validator::make($input, [
            'url' => 'required|string|unique:videos,id,'.$id.'|min:10|max:120',
            'title' => 'required|string|max:120',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }

        $video->url = $input['url'];
        $video->title = $input['title'];
        $video->description = $input['description'];
        $video->save();

        return $this->sendResponse($video, 'La video '.$video->name.'a été bien ajouter.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::find($id);
        if (is_null($video))
            return $this->sendError('Video not found.');
        $objet = $video->name_en;
        Video::destroy($id);
        return $this->sendResponse($objet, 'La video '.$objet.' a été supprimer.');
    }
}
