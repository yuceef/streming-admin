<?php

namespace App\Http\Controllers;

use App\League;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class LeagueController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leagues = League::all();
        return $this->sendResponse($leagues->toArray(), 'Leagues retrieved successfully.');
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $orderBy = (isset($input['orderBy']))?$input['orderBy']:'name_en';
        $orderByAsc = (isset($input['orderByAsc']))?$input['orderByAsc']:'asc';
        $page = (isset($input['page']))?$input['page']-1:0;
        $limit = (isset($input['limit']))?$input['limit']:10;
        $searchBy = (isset($input['searchBy']))?$input['searchBy']:false;
        $searchValue = (isset($input['searchValue']))?$input['searchValue']:false;
        $repLeague = array();
        $Leagues = League::orderBy($orderBy, $orderByAsc)
                        ->where(function ($query) use($searchBy, $searchValue){
                            if($searchBy and $searchValue)
                            $query->where($searchBy, 'like', "%".$searchValue."%");
                        })
                        ->offset($page * $limit)
                        ->limit($limit)
                        ->get();
        $total = League::orderBy($orderBy, $orderByAsc)
                            ->where(function ($query) use($searchBy, $searchValue){
                                if($searchBy and $searchValue)
                                    $query->where($searchBy, 'like', "%".$searchValue."%");
                            })
                            ->count();
        return $this->sendResponse($Leagues->toArray(), $total);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name_ar' => 'required|string|unique:leagues|max:120',
            'name_en' => 'required|string|unique:leagues|max:120',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }

        $filename = $_FILES['logo']['name'];        
        $path = $request->file('logo')->storeAs(
            'public/leagues', $request->name_en."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
        );

        $league = new League;
        $league->name_ar = $input['name_ar'];
        $league->name_en = $input['name_en'];
        $league->saison = $input['saison'];
        $league->logo = str_replace("public/leagues","/storage/leagues",$path);
        $league->description = $input['description'];
        $league->save();

        return $this->sendResponse($league, 'la ligue '.$league->name.'a été bien ajouter.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $league = League::find($id);
        if (is_null($league))
            return $this->sendError('League not found.');
        return $this->sendResponse($league->toArray(), 'la ligue '.$league->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function edit(League $league)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $league = League::find($id);
        if (is_null($league))
            return $this->sendError('League not found.');
        $input = $request->all();
        $validator = Validator::make($input, [
            'name_ar' => 'required|string|unique:leagues,id,'.$id.'|max:120',
            'name_en' => 'required|string|unique:leagues,id,'.$id.'|max:120',
            'description' => 'required|string|min:5'
        ]);

        if($validator->fails()){
            return $this->sendError('Errors', $validator->errors(), 502);       
        }

        if(isset($_FILES['logo'])){
            $filename = $_FILES['logo']['name'];
            $path = $request->file('logo')->storeAs(
                'public/leagues', $request->name_en."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
            );
            $league->logo = str_replace("public/leagues","/storage/leagues",$path);
        }

        $league->name_ar = $input['name_ar'];
        $league->name_en = $input['name_en'];
        $league->saison = $input['saison'];
        $league->description = $input['description'];
        $league->save();

        return $this->sendResponse($league, 'la ligue '.$league->name.'a été bien ajouter.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $league = League::find($id);
        if (is_null($league))
            return $this->sendError('League not found.');
        $objet = $league->name_en;
        League::destroy($id);
        return $this->sendResponse($objet, 'la ligue '.$objet.' a été supprimer.');
    }
}
