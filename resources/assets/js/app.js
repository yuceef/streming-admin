
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import App from './App.vue';
import Home from './components/Home.vue';
import addTeam from './components/team/addTeam.vue';
import teams from './components/team/teams.vue';
import addLeague from './components/league/addLeague.vue';
import leagues from './components/league/leagues.vue';
import addChannel from './components/channel/addChannel.vue';
import channels from './components/channel/channels.vue';
import addVideo from './components/video/addVideo.vue';
import genererVideos from './components/video/genererVideos.vue';
import videos from './components/video/videos.vue';
import addMatch from './components/match/addMatch.vue';
import matchs from './components/match/matchs.vue';

const routes = [
    {
        name: 'Home',
        path: '/',
        component: Home
    },
    {
        name: 'addTeam',
        path: '/addTeam',
        component: addTeam
    },
    {
        name: 'teams',
        path: '/teams',
        component: teams
    },
    {
        name: 'addLeague',
        path: '/addLeague',
        component: addLeague
    },
    {
        name: 'leagues',
        path: '/leagues',
        component: leagues
    },
    {
        name: 'addChannel',
        path: '/addChannel',
        component: addChannel
    },
    {
        name: 'channels',
        path: '/channels',
        component: channels
    },
    {
        name: 'addVideo',
        path: '/addVideo',
        component: addVideo
    },
    {
        name: 'videos',
        path: '/videos',
        component: videos
    },
    {
        name: 'genererVideos',
        path: '/genererVideos',
        component: genererVideos
    },
    {
        name: 'addMatch',
        path: '/addMatch',
        component: addMatch
    },
    {
        name: 'matchs',
        path: '/matchs',
        component: matchs
    }
];

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount('#appFront');