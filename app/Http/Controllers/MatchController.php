<?php

namespace App\Http\Controllers;

use App\Match;
use App\League;
use App\Team;
use App\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class MatchController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matchs = Match::all();
        return $this->sendResponse($matchs->toArray(), 'Matchs retrieved successfully.');
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $orderBy = (isset($input['orderBy']))?$input['orderBy']:'date';
        $orderByAsc = (isset($input['orderByAsc']))?$input['orderByAsc']:'desc';
        $page = (isset($input['page']))?$input['page']-1:0;
        $limit = (isset($input['limit']))?$input['limit']:10;
        $searchBy = (isset($input['searchBy']))?$input['searchBy']:false;
        $searchValue = (isset($input['searchValue']))?$input['searchValue']:false;
        $repMatch = array();
        $Matchs = Match::orderBy($orderBy, $orderByAsc)
                        ->where(function ($query) use($searchBy, $searchValue){
                            if($searchBy and $searchValue)
                            $query->where($searchBy, 'like', "%".$searchValue."%");
                        })
                        ->offset($page * $limit)
                        ->limit($limit)
                        ->get();
        $total = Match::orderBy($orderBy, $orderByAsc)
                            ->where(function ($query) use($searchBy, $searchValue){
                                if($searchBy and $searchValue)
                                    $query->where($searchBy, 'like', "%".$searchValue."%");
                            })
                            ->count();
        foreach ($Matchs as $match) {
            $match->league = League::find($match->league_id);
            $match->home = Team::find($match->home_team);
            $match->away = Team::find($match->away_team);
        }
        return $this->sendResponse($Matchs->toArray(), $total);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'home_team' => 'required',
            'away_team' => 'required',
            'league_id' => 'required',
            'dateM' => 'required',
            'timeM' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        $input['date'] =$input['dateM']." ".$input['timeM'];
        $match = new Match;
        $match->home_team = $input['home_team'];
        $match->away_team = $input['away_team'];
        $match->description = $input['description'];
        if(isset($input['home_goal']))$match->home_goal = $input['home_goal'];
        if(isset($input['away_goal']))$match->away_goal = $input['away_goal'];
        $match->league_id = $input['league_id'];
        $match->date = date($input['date']);
        $match->channels = implode(",",$input['channels']);
        if(isset($input['video']))$match->video = $input['video'] ;
        if(isset($input['stadium']))$match->stadium = $input['stadium'] ;
        if(isset($input['isExtra']))$match->isExtra = $input['isExtra'] ;
        if(isset($input['isPenalty']))$match->isPenalty = $input['isPenalty'] ;
        if(isset($input['day']))$match->day = $input['day'] ;
        $match->save();

        return $this->sendResponse($match, 'Le match '.$match->name.'a été bien ajouter.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $match = Match::find($id);
        if (is_null($match))
            return $this->sendError('Match not found.');
        $match->vue += 1;
        $match->save();
        $match->league = League::find($match->league_id);
        $match->home = Team::find($match->home_team);
        $match->away = Team::find($match->away_team);
        $infoChannel = array();
        $match->channels = explode(",",$match->channels);
        foreach ($match->channels as $channel) {
            $infoChannel[] = Channel::find($channel);
        }
        $match->infoChannel=$infoChannel;
        return $this->sendResponse($match->toArray(), 'Le match '.$match->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function edit(Match $match)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $match = Match::find($id);
        if (is_null($match))
            return $this->sendError('Match not found.');
        
        $input = $request->all();
        $validator = Validator::make($input, [
            'home_team' => 'required',
            'away_team' => 'required',
            'league_id' => 'required',
            'description' => 'required',
            'dateM' => 'required',
            'timeM' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        $input['date'] =$input['dateM']." ".$input['timeM'];
        $match->home_team = $input['home_team'];
        $match->away_team = $input['away_team'];
        if(isset($input['home_goal']))$match->home_goal = $input['home_goal'];
        if(isset($input['away_goal']))$match->away_goal = $input['away_goal'];
        $match->league_id = $input['league_id'];
        $match->description = $input['description'];
        $match->date = date($input['date']);
        $match->channels = implode(",",$input['channels']);
        if(isset($input['video']))$match->video = $input['video'] ;
        if(isset($input['stadium']))$match->stadium = $input['stadium'] ;
        if(isset($input['isExtra']))$match->isExtra = $input['isExtra'] ;
        if(isset($input['isPenalty']))$match->isPenalty = $input['isPenalty'] ;
        if(isset($input['day']))$match->day = $input['day'] ;
        $match->save();

        return $this->sendResponse($match, 'Le match '.$match->name.'a été bien ajouter.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $match = Match::find($id);
        if (is_null($match))
            return $this->sendError('Match not found.');
        $objet = $match->name_en;
        Match::destroy($id);
        return $this->sendResponse($objet, 'Le match '.$objet.' a été supprimer.');
    }

}
